export const request = async <T>(url: string): Promise<T>  => {
    return await fetch(`https://restcountries.eu/rest/v2/${url}`)
            .then(response => response.json())
            .catch(error => error)
}