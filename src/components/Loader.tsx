import React, { FC } from 'react';
import BeatLoader from "react-spinners/BeatLoader";

const Loader: FC = () => (
    <BeatLoader 
        size={15}
        color='gray'
    />
)

export default Loader;