import React, { FC } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Regions from '../pages/Regions';
import Countries from '../pages/Countries';
import Country from '../pages/Country';

const App: FC = () => (
    <Router>
        <Switch>
          <Route path='/regions' component={Regions} exact />
          <Route path='/:region/countries' component={Countries} exact />
          <Route path='/:region/countries/:country' component={Country} exact />
          <Redirect to='/regions' />
        </Switch>
    </Router>
)

export default App;