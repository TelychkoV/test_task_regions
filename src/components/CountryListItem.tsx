import React, { FC, ReactElement } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { ICountryListItemProps } from '../types/countries';
import { IRegion } from '../types/regions';

const CountryListItem: FC<ICountryListItemProps | any> = ({ start, base, countries }: ICountryListItemProps) => {
    const history = useHistory();
    const match = useRouteMatch();
    
    const showCountryInfo = (country: IRegion): void => history.push(`${match.url}/${country.name}`, country)
    
    return (
        countries
        .slice(start === 1 ? 0 : (start - 1) * base, base * start)
        .map((country: IRegion): ReactElement => (
            <tr key={country.alpha3Code} 
                style={{ cursor: 'pointer' }}
                onClick={() => showCountryInfo(country)}>
                <td>{country.alpha3Code}</td>
                <td>{country.name}</td>
                <td>{country.density} per km2</td>
                <td>{country.capital}</td>
            </tr>
            )
        )
    )
}

export default CountryListItem;