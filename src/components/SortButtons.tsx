import React, { FC, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getCountriesRequest } from '../redux/middlewares/getCountries.request';
import { sortByDensityAction, sortByNameAction } from '../redux/actions/getCountries.action';
import { useDispatch } from "react-redux";
import { Button } from 'react-bootstrap';
import '../styles/SortButtons.css';

const SortButtons: FC = () => {
    const dispatch = useDispatch();
    const { region } = useParams();
    const [ nameStatusSort, setNameStatus ] = useState<boolean>(false);
    const [ densityStatusSort, setDensityStatus ] = useState<boolean>(false);
  
    const sortByName = (): void => {
        setNameStatus(!nameStatusSort);
        dispatch(sortByNameAction(!nameStatusSort));
    }
    const sortByDensity = (): void => {
        setDensityStatus(!densityStatusSort);
        dispatch(sortByDensityAction(!densityStatusSort));
    }
    
    const showOriginalList = () => dispatch(getCountriesRequest(region.toLowerCase()));
    
    return (
        <section className='buttons'>
            <Button variant="primary" onClick={sortByName}>
                Sort by Name 
                {nameStatusSort? ' asc' : ' desc'}
            </Button> 
            <Button variant="primary" onClick={sortByDensity}>
                Sort by Density 
                {densityStatusSort? ' asc' : ' desc'}
            </Button> 
            <Button variant="primary" onClick={showOriginalList}>Original List</Button> 
        </section>
       
    )
}

export default SortButtons;