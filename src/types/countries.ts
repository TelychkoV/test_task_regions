import { IRegion } from './regions';

export interface ICountryListItemProps {
    start: number;
    base: number;
    countries: IRegion;
}