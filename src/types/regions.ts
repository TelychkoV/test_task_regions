interface ICurrency {
    code: string;
    name: string;
    symbol: string;
}

export interface IRegion {
    alpha2Code: string;
    area: number;
    capital: string;
    currencies: ICurrency[];
    flag: string;
    name: string;
    population: number;
    region: string;
    timezones: string[];
    topLevelDomain: string[];
    [propName: string]: any;
}