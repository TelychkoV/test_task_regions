import { 
    GET_COUNTRIES_REQUEST,
    GET_COUNTRIES_REQUEST_SUCCESS,
    GET_COUNTRIES_REQUEST_FAILURE,
    SORT_BY_DENSITY,
    SORT_BY_NAMES,
    CLEAR_COUNTRIES
} from '../redux/constants/countries.constants';
import { IRegion } from './regions';

interface GetCountriesAction {
    type: typeof GET_COUNTRIES_REQUEST;
}
  
interface GetCountriesSuccessAction {
    type: typeof GET_COUNTRIES_REQUEST_SUCCESS;
    payload?: IRegion[];
}

interface GetCountriesFailureAction {
    type: typeof GET_COUNTRIES_REQUEST_FAILURE
    error?: string
}

interface SortByNamesAction {
    type: typeof SORT_BY_NAMES,
    status?: boolean
}

interface SortByDensityAction {
    type: typeof SORT_BY_DENSITY,
    status?: boolean
}

interface ClearCountriesAction {
    type: typeof CLEAR_COUNTRIES
}

export interface ICountriesState {
    loading: boolean;
    countries?: null | IRegion[];
    error?: null | string
}

export type CountryActionTypes = 
    GetCountriesAction | 
    GetCountriesSuccessAction | 
    GetCountriesFailureAction | 
    SortByNamesAction | 
    SortByDensityAction | 
    ClearCountriesAction;