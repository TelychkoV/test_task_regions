import { 
    GET_REGIONS_REQUEST, 
    GET_REGIONS_REQUEST_SUCCESS, 
    GET_REGIONS_REQUEST_FAILURE
} from '../redux/constants/regions.constants';
import { IRegion } from './regions';

interface GetRegionsAction {
    type: typeof GET_REGIONS_REQUEST
}
  
interface GetRegionsSuccessAction {
    type: typeof GET_REGIONS_REQUEST_SUCCESS
    payload?: IRegion[]
}

interface GetRegionsFailureAction {
    type: typeof GET_REGIONS_REQUEST_FAILURE
    error?: string
}

export interface IRegionsState {
    loading: boolean;
    regions?: string[] | null;
    error?: null | string
}

export type RegionActionTypes = GetRegionsAction | GetRegionsSuccessAction | GetRegionsFailureAction;