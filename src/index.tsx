import React, { StrictMode } from 'react';
import { Provider } from 'react-redux'
import { render } from 'react-dom';
import store from './redux/store';
import App from './components/App';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/index.css';
import * as serviceWorker from './serviceWorker';

render(
  <Provider store={store}>
    <StrictMode>
      <App />
    </StrictMode>
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();
