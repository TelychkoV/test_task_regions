import { combineReducers } from 'redux';
import RegionsReducer from './reducers/getRegions.reducer';
import CountriesReducer from './reducers/getCountries.reducer';

const rootReducer = combineReducers({
    RegionsReducer,
    CountriesReducer
});

export default rootReducer;