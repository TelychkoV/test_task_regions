import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from './combineReducers';

const logger = createLogger({ collapsed: true }),
      createStoreWithMiddleware = applyMiddleware(thunk, logger),
      store = createStore(rootReducer, compose(createStoreWithMiddleware));

export default store;