import { 
    GET_COUNTRIES_REQUEST,
    GET_COUNTRIES_REQUEST_SUCCESS,
    GET_COUNTRIES_REQUEST_FAILURE,
    CLEAR_COUNTRIES,
    SORT_BY_DENSITY,
    SORT_BY_NAMES
} from '../constants/countries.constants';
import { CountryActionTypes } from '../../types/countries.redux';
import { IRegion } from '../../types/regions';

export const getCountriesAction = (): CountryActionTypes => ({ type: GET_COUNTRIES_REQUEST });
export const getCountriesSuccessAction = (payload: IRegion[]): CountryActionTypes => ({ type: GET_COUNTRIES_REQUEST_SUCCESS, payload });
export const getCountriesFailureAction = (error: string): CountryActionTypes => ({ type: GET_COUNTRIES_REQUEST_FAILURE, error });
export const clearCountriesAction = (): CountryActionTypes => ({ type: CLEAR_COUNTRIES });
export const sortByDensityAction = (status: boolean): CountryActionTypes => ({ type: SORT_BY_DENSITY, status });
export const sortByNameAction = (status: boolean): CountryActionTypes => ({ type: SORT_BY_NAMES, status });