import { 
    GET_REGIONS_REQUEST, 
    GET_REGIONS_REQUEST_SUCCESS, 
    GET_REGIONS_REQUEST_FAILURE 
} from '../constants/regions.constants';
import { RegionActionTypes } from '../../types/regions.redux';
import { IRegion } from '../../types/regions';

export const getRegionsAction = (): RegionActionTypes => ({ type: GET_REGIONS_REQUEST });
export const getRegionsSuccessAction = (payload: IRegion[]): RegionActionTypes => ({ type: GET_REGIONS_REQUEST_SUCCESS, payload });
export const getRegionsFailureAction = (error: string): RegionActionTypes => ({ type: GET_REGIONS_REQUEST_FAILURE, error });