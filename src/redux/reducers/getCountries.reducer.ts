import { 
  GET_COUNTRIES_REQUEST, 
  GET_COUNTRIES_REQUEST_SUCCESS, 
  GET_COUNTRIES_REQUEST_FAILURE,
  CLEAR_COUNTRIES,
  SORT_BY_NAMES,
  SORT_BY_DENSITY
} from '../constants/countries.constants';

import { ICountriesState, CountryActionTypes } from '../../types/countries.redux';

const initialState: ICountriesState = {
  loading: false,
  countries: null,
  error: null
};

const CountriesReducer = (state = initialState, action: CountryActionTypes): ICountriesState => {
  switch (action.type) {
    case GET_COUNTRIES_REQUEST:
      return  {
        ...state,
        loading: true
      };
    case GET_COUNTRIES_REQUEST_SUCCESS:
      action.payload = action.payload?.map(data => {
        data.density = +(data.area / data.population * 1000).toFixed(2);
        return data;
      })
      return {
        ...state,
        countries: action.payload,
        loading: false
      };
    case GET_COUNTRIES_REQUEST_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    case CLEAR_COUNTRIES:
      return {
        ...state,
        countries: null
      }  
    case SORT_BY_NAMES:
      return {
        ...state,
        countries: state.countries?.sort((a, b) => {
          if(action.status) {
            if (a.name > b.name) {
              return 1;
            }
            if (a.name < b.name) {
              return -1;
            }
          } else {
            if (a.name < b.name) {
              return 1;
            }
            if (a.name > b.name) {
              return -1;
            }
          }
            return 0;
          }
        )
      }  
    case SORT_BY_DENSITY: 
      return {
        ...state,
        countries: state.countries?.sort((a, b) => action.status ? a.density - b.density : b.density - a.density)
      }
    default:
      return state;
  }
};

export default CountriesReducer;