import { GET_REGIONS_REQUEST, GET_REGIONS_REQUEST_SUCCESS, GET_REGIONS_REQUEST_FAILURE } from '../constants/regions.constants';
import { RegionActionTypes, IRegionsState } from '../../types/regions.redux';
import { IRegion } from '../../types/regions';

const initialState: IRegionsState = {
  loading: false,
  regions: null,
  error: null
};

const RegionsReducer = (state = initialState, action: RegionActionTypes): IRegionsState => {
  switch (action.type) {
    case GET_REGIONS_REQUEST:
      return  {
        ...state,
        loading: true
      };
    case GET_REGIONS_REQUEST_SUCCESS:
      const regions: string[] = [];
      action.payload?.forEach((data: IRegion): void => {
          if(!regions.includes(data.region) && data.region.length) {
            regions.push(data.region);
          }
        });
      return {
        ...state,
        regions,
        loading: false
      };
    case GET_REGIONS_REQUEST_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      return state;
  }
};

export default RegionsReducer;