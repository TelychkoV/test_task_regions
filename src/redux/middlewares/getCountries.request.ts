import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { RootState } from '../../types/general';
import { getCountriesAction, getCountriesSuccessAction, getCountriesFailureAction } from '../actions/getCountries.action';
import { request } from '../../api/request';

export const getCountriesRequest = (param: string): ThunkAction<void, RootState, unknown, Action<string>> => 
    dispatch => {
      dispatch(getCountriesAction());
      return request(`region/${param}`)
        .then((data: any) => dispatch(getCountriesSuccessAction(data)))
        .catch((error: string) => dispatch(getCountriesFailureAction(error)))
}