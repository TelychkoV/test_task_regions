import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { RootState } from '../../types/general';
import { getRegionsAction, getRegionsSuccessAction, getRegionsFailureAction } from '../actions/getRegions.action';
import { request } from '../../api/request';

export const getRegionsRequest = (): ThunkAction<void, RootState, unknown, Action<string>> => 
    dispatch => {
      dispatch(getRegionsAction());
      return request('all')
        .then((data: any) => dispatch(getRegionsSuccessAction(data)))
        .catch((error: string) => dispatch(getRegionsFailureAction(error)))
}