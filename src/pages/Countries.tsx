import React, { FC, useState, useEffect, ReactElement, MouseEvent } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { getCountriesRequest } from '../redux/middlewares/getCountries.request';
import { clearCountriesAction } from '../redux/actions/getCountries.action';
import { useDispatch } from "react-redux";
import Loader from '../components/Loader';
import { Table, Pagination, Button } from 'react-bootstrap';
import CountryListItem from '../components/CountryListItem';
import SortButtons from '../components/SortButtons';
import { ICountriesState } from '../types/countries.redux';
import { useSelector } from '../types/general';
import { IRegion } from '../types/regions';
import '../styles/Countries.css';

const Countries: FC = () => {
    const dispatch = useDispatch();
    const { loading, countries, error }: ICountriesState = useSelector(state => state.CountriesReducer);
    const history = useHistory();
    const { region } = useParams();
    const [ start, toSlice ] = useState<number>(1);
    const base: number = 15;
    const titles: string[] = ['Code', 'Name', 'Density', 'Capital'];

    useEffect(() => {
        dispatch(getCountriesRequest(region.toLowerCase()));
    }, [dispatch, region]);

    if(error) {
        return <p>Error</p>;
    }

    const choosePagData = (event: MouseEvent<HTMLDivElement>) => {
        const target = event.target as HTMLDivElement;
        toSlice(parseInt(target.innerText));
    };

    const setPagination = (data: IRegion[]): ReactElement[] => {   
        let items: ReactElement[] = [], 
            number: number, 
            total: number = Math.round(data && data.length / base);

        for (number = 1; number <= total; number += 1) {        
            items.push(
                <Pagination.Item  
                    onClick={choosePagData} 
                    key={number} 
                    active={number === start}>
                        {number}
                </Pagination.Item>,
            )
        }
        return items;
    }   

    const backToRegions = (): void => {
        dispatch(clearCountriesAction());
        history.push('/regions');
    }

    return (
        <>
        <h1 className='title'>
             COUNTRIES IN {region.toUpperCase()}
             {loading && <Loader />}
        </h1>
        <SortButtons />
        {countries && 
        <>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        {titles.map((title, index) => <th key={index}>{title}</th>)}
                    </tr>
                </thead>
                <tbody>
                    <CountryListItem 
                        start={start}
                        base={base}
                        countries={countries}
                    />
                </tbody>
            </Table>
            <section className='footer'>
                <Pagination>{setPagination(countries)}</Pagination>
                <Button variant="primary" onClick={backToRegions}>Back to regions</Button> 
            </section>
        </>    
        }
        </>
       
    )
}

export default Countries;