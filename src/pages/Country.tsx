import React, { FC } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { IRegion } from '../types/regions';
import '../styles/Country.css';

const Country: FC = () => {
    const history = useHistory();
    const { state }: { state: IRegion } = useLocation(); 
    
    const backToRegions = (): void => history.push('/regions');
    const backToCountries = (): void => history.goBack();
    
    return (
        <>
            <h1 className='title_country'>{state.name.toUpperCase()}</h1>
            <div className='country'>
                <section>
                    <img src={state.flag} alt='flag' className='flag' />
                </section>
                <section>
                    <ul>
                        <li>
                            <span>Capital</span>: {state.capital}
                        </li>
                        <li>
                            <span>Area</span>: {state.area} km2
                        </li>
                        <li>
                            <span>Density</span>: {state.density} per km2
                        </li>
                        <li>
                            <span>Code</span>: {state.alpha2Code}
                        </li>
                    </ul>
                </section>
                <section>
                    <ul>
                        <li>
                            <span>Population</span>: {state.population / 1000000} millions
                        </li>
                        <li>
                            <span>Timezone</span>: {state.timezones[0]}
                        </li>
                        <li>
                            <span>Domain</span>: {state.topLevelDomain[0]}
                        </li>
                        <li>
                            <span>Currency</span>: {state.currencies[0].code}
                        </li>
                    </ul>
                </section>
            </div>
            <section className='nav_buttons'>
                <Button variant="primary" onClick={backToCountries} className='countries_back'>Back to countries</Button> 
                <Button variant="primary" onClick={backToRegions}>Back to regions</Button> 
            </section>
        </>
    )
}

export default Country;