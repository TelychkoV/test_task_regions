import React, { useEffect, FC, ReactElement } from 'react';
import { Link } from 'react-router-dom';
import { getRegionsRequest } from '../redux/middlewares/getRegions.request';
import { useDispatch } from "react-redux";
import Loader from '../components/Loader';
import { useSelector } from '../types/general';
import { IRegionsState } from '../types/regions.redux';
import '../styles/Regions.css';

const Regions: FC = () => {
    const dispatch = useDispatch();
    const { loading, regions, error }: IRegionsState = useSelector(state => state.RegionsReducer);

    useEffect(() => {
        dispatch(getRegionsRequest());
    }, [dispatch]);

    if(error) {
        return <p>Error</p>;
    }
    
    return (
        <>
         <h1 className='title'>
             WORLD REGIONS
             {loading && <Loader />}
        </h1>
         
         <section className='regions'>
            {regions && regions?.map((region: string, index: number): ReactElement => ( 
                <Link
                    to={`/${region}/countries`} 
                    className={`region region-${index}`}
                    key={index}>
                    {region}
                </Link>  
            )
            )}
         </section>
        </>
       
    )
}

export default Regions;